const router = require('koa-router')()
const User = require('../controllers/user')
const GitHub = require('../controllers/github')
const Article = require('../controllers/article')
const Tag = require('../controllers/tag')
const Log = require('../controllers/log')
const Comment = require('../controllers/comment')
const productToken  = require('../utils/produceToken')
const middleWares = require('../middlewares')

router
	.get('/', async ctx => {
		console.log(productToken.getCode())
		await ctx.render('index', {
			name: ctx.state.username
		})
	})
	.get('/info', async ctx => {
		ctx.body = {
			author: 'evilemon',
			profession: 'FE',
			version: '0.0.1',
			github: 'https://github.com/evilemon',
			site: 'https://energys.cn'
		}
	})
	.get('/login', async ctx => {
		await ctx.render('login', {
			title: '登录'
		})
	})
	.get('/register', async ctx => {
		await ctx.render('register', {
			title: '注册'
		})
	})
	.post('/register', User.register)
	.post('/login', User.login)
	.get('/logout', User.logout)
	.get('/getCaptcha', User.getCaptcha)
	.get('/github', GitHub.list)
	.post('/article/create', Article.createArticle)
	.delete('/article/delete/:id', Article.deleteArticle)
	.put('/article/update/:id', Article.updateArticle)
	.get('/article/serv/list', Article.getArticlesList)
	.get('/article/serv/detail/:id', Article.getArticle)
	.get('/article/list', Article.getFrontArticlesList)
	.get('/article/detail/:id', Article.getFrontArticle)
	.get('/article/counts', Article.getFrontArticlesCount)
	.post('/tag/create', Tag.addTag)
	.put('/tag/update/:id', Tag.updateTag)
	.delete('/tag/delete/:id', Tag.deleteTag)
	.get('/tag/list', Tag.getFrontTagsList)
	.get('/tag/serv/list', Tag.getTagList)
	.get('/logs/list', Log.getLogsList)
	.post('/comment/create/:id', Comment.addComment)
	.delete('/comment/delete/:cid', Comment.deleteComment)

module.exports = router