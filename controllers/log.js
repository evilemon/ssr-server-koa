/**
 * 操作记录
 */

const mongoose = require('mongoose')
const Log = mongoose.model('Log')
const logType = require('../enums/log_type')

const getLogsList = async(ctx, next) => {
	let currentPage = (parseInt(ctx.query.currentPage) > 0) ? parseInt(ctx.query.currentPage) : 1
	let perPage = (parseInt(ctx.query.perPage) > 0) ? (parseInt(ctx.query.perPage) > 0) : 10
	let startRow = (currentPage - 1) * perPage
	try {
		const list = await Log.find().skip(startRow).limit(perPage).sort('create_time')
		const count = await Log.count()
		ctx.body = {
			success: true,
			list,
			count
		}
	} catch (e) {
		ctx.throw(400, '获取列表失败')
	}
}

/**
 * log utils
 */

const createLog = async(person, type, content) => {
	await new Log({
		uid: person,
		type,
		content
	}).save()
}

module.exports = {
	getLogsList,
	createLog
}