/**
 * 用于获取用户的github信息
 */

const redis = require('../db/redis')

const list = async ctx => {
	let repos = await redis.getItem('github-repos')

	ctx.body = {
		success: true,
		message: '获取github项目列表',
		data: {
			repos
		}
	}
}

module.exports = {
	list
}