/**
 * @file baiduSeo
 */

const axios = require('axios')
const config = require('../config')

const baiduSeoPush = urls => {
	const body = Array.isArray(urls) ? urls : [urls]
	axios({
		url: `http://data.zz.baidu.com/urls?site=${config.mysite}&token=${config.baiduSeoToken}`,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
		data: body.join('\n'),
		method: 'post'
	}).then(res => {
		console.log(res)
	}).catch(err => {
		console.log(err)
	})
}

const baiduSeoUpdate = urls => {
	const body = Array.isArray(urls) ? urls : [urls]
	axios({
		url: `http://data.zz.baidu.com/update?site=${config.mysite}&token=${config.baiduSeoToken}`,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
		data: body.join('\n'),
		method: 'post'
	}).then(res => {
		console.log(res)
	}).catch(err => {
		console.log(err)
	})
}

const baiduSeoDelete = urls => {
	const body = Array.isArray(urls) ? urls : [urls]
	axios({
		url: `http://data.zz.baidu.com/del?site=${config.mysite}&token=${config.baiduSeoToken}`,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
		data: body.join('\n'),
		method: 'post'
	}).then(res => {
		console.log(res)
	}).catch(err => {
		console.log(err)
	})
}

module.exports = {
	baiduSeoPush,
	baiduSeoUpdate,
	baiduSeoDelete
}
