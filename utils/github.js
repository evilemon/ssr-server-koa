/**
 * 爬取github信息
 */

const { GITHUB } = require('../config')
const redis = require('../db/redis')
const axios = require('axios')

const prefix = 'https://api.github.com'

const getGitHubRepos = async () => {
	try {
		// https://developer.github.com/v3/repos/#list-user-repositories
		const result = await axios({
			url: `${prefix}/users/${GITHUB.account}/repos`
		})
		redis.setItem('github-repos', result.data)
		console.log('github 账户repo爬取成功')
	} catch (e) {
		console.log(e)
	}	
	setTimeout(getGitHubRepos, 1000 * 60 * 60 * 12 )
}

module.exports = getGitHubRepos