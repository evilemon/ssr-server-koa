/**
 * product token
 */

const uuid4 = require('uuid/v4')

class ProduceCode {
	constructor() {
		this.verifyCode = uuid4()
		this.timer = null
	}
	setCode() {
		this.verifyCode = uuid4()
	}
	getCode() {
		return this.verifyCode
	}
	start() {
		this.timer = setInterval(() => {
			this.setCode()
		}, 60 * 30 * 1000 )
	}
	stop() {
		this.timer = null
	}
}

const pc = new ProduceCode()
pc.start()

module.exports = pc