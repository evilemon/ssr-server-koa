const github = require('./github')
const produceToken = require('./produceToken')
const baiduSeo = require('./baiduSeo')
const email = require('./email')
const akismet = require('./akismet')

module.exports = {
	github,
	produceToken,
	baiduSeo,
	email,
	akismet
}