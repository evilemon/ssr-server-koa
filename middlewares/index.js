/**
 * 统一处理错误和签名token
 */

const verifyToken = require('./verifyToken')
const errorHandler = require('./errorHandler')

module.exports = {
	verifyToken,
	errorHandler
}