/**
 * 
 */

const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = Schema.ObjectId

const TagSchema = new Schema({
	name: {
		//标签名称
		type: String,
		unique: true
	},
	is_show: {
		type: Boolean,
		default: true
	},
	// 创建时间
	create_time: {
		type: Date,
		default: Date.now()
	},

	// 修改时间
	update_time: {
		type: Date
	},
	// 扩展属性
	extends: [{
		name: {
			type: String,
			validate: /\S+/
		},
		value: {
			type: String,
			validate: /\S+/
		}
	}]
})

// 更新修改时间
TagSchema.pre('save', function(next) {
	if (this.isNew) {
		this.update_time = this.create_time = Date.now()
	} else {
		this.update_time = Date.now()
	}
	next()
})

TagSchema.pre('findOneAndUpdate', function(next) {
	this.findOneAndUpdate({}, {
		update_time: Date.now()
	})
	next()
})

mongoose.model('Tag', TagSchema)