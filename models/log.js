const mongoose = require('mongoose')
const Schema = mongoose.Schema
const ObjectId = Schema.ObjectId

const LogSchema = new Schema({
	uid: {
		type: ObjectId,
		ref: 'User'
	},
	content: {
		type: String,
		require: true,
		trim: true
	},
	type: {
		type: String,
		require: true
	},
	create_time: {
		type: Date,
		default: Date.now()
	}
})

LogSchema.pre('save', function(next) {
	if (this.isNew) {
		this.update_time = this.create_time = Date.now()
	} else {
		this.update_time = Date.now()
	}
	next()
})
LogSchema.pre('findOneAndUpdate', function(next) {
	this.findOneAndUpdate({}, {
		update_time: Date.now()
	})
	next()
})

mongoose.model('Log', LogSchema)