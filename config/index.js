/**
 * project config
 */

const development = {
	mongodb: {
		user: 'slm',
		pass: '123456',
		host: '127.0.0.1',
		port: 27017,
		database: 'myblog'
	}
}

const production = {
	mongodb: {
		user: 'slm',
		pass: '123456',
		host: '127.0.0.1',
		port: 27017,
		database: 'myblog'
	}
}

const port = 8090

const redisKey = 'KoaBlog'

const defaultUser = {
	name: 'admin',
	pass: 'admin'
}

const mysite = 'energys.cn'
const baiduSeoToken = 'k9tbDNa5sevIkrUT'

const GITHUB = {
	account: 'evilemon'
}

const EMAIL = {
	account: '1161735171@qq.com',
	password: 'ldflawcnuqaybaeg'
}

const AKISMET = {
	key: '6026788e2615',
    blog: 'energys.cn'
}

module.exports = {
	development,
	production,
	port,
	defaultUser,
	redisKey,
	mysite,
	GITHUB,
	baiduSeoToken,
	EMAIL,
	AKISMET
}